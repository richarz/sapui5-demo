sap.ui.define([
	"sap/ui/core/util/MockServer",
	"sap/base/util/UriParameters"
], function (MockServer, UriParameters) {

	return {
		init: function () {
			// create
			var oMockServer = new MockServer({
				rootUri: "https://sap.dal.de/SomeService/"
			})

			var oUriParameters = new UriParameters(window.location.href)

			// configure mock server with a delay
			MockServer.config({
				autoRespond: true,
				autoRespondAfter: oUriParameters.get("serverDelay") || 500
			})

			// simulate
			var sPath = sap.ui.require.toUrl("demo/localService")
			oMockServer.simulate(sPath + "/metadata.xml", sPath + "/mockdata")

			// start
			oMockServer.start()
		}
	}

})