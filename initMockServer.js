sap.ui.define([
    "demo/localService/mockserver"
], function(mockserver) {
    
    mockserver.init()

    sap.ui.require(["sap/ui/core/ComponentSupport"])
})