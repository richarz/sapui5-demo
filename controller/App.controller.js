sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
], (Controller, Filter, FilterOperator) => {

    return Controller.extend("demo.controller.App", {
        toDetail: function(oEvent) {
            const oListItem = oEvent.getSource()
            const sPath = oListItem.getBindingContext('firms').getPath().substr(1)
            this.getOwnerComponent().getRouter().navTo('detail', {
                id: sPath
            })
        },

        doSearch: function(oEvent) {
            const aFilter = []
            const oList = this.getView().byId('firmsList')
            const sSearchQuery = oEvent.getSource().getValue()

            if(sSearchQuery) {
                // create new filter
                aFilter.push(
                    new Filter('FirmIdent', FilterOperator.Contains, sSearchQuery)
                )
            }

            // apply filter to binding
            oList.getBinding('items').filter(aFilter)
        }
    })
})