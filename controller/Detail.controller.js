sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History"
], (Controller, History) => {

    return Controller.extend("demo.controller.App", {
        onInit: function() {
            var oRouter = this.getOwnerComponent().getRouter();
			oRouter.getRoute("detail").attachMatched(this.onRouteMatched, this)
        },

        onRouteMatched: function(oEvent) {
            const sPath = oEvent.getParameter('arguments').id
            const oView = this.getView()

            oView.bindElement({
                path: `/${window.decodeURIComponent(sPath)}`,
                model: 'firms',
                events : {
					dataRequested: function () {
						oView.setBusy(true)
					},
					dataReceived: function () {
						oView.setBusy(false)
					}
				}
            })
        },

        onNavBack: function () {
			const oHistory = History.getInstance()
			const sPreviousHash = oHistory.getPreviousHash()

			if (sPreviousHash !== undefined) {
				window.history.go(-1)
			} else {
				const oRouter = this.getOwnerComponent().getRouter()
				oRouter.navTo("main", {}, true)
			}
		},

        formatStatus: function(iValue) {
            const oStatus = {
                1: 'Kunde',
                2: 'Kein Kunde',
                3: 'Altkunde'
            }

            return oStatus[iValue]
        },

        formatStatusColor: function(iValue) {
            const oStatus = {
                1: 'Success',
                2: 'Error',
                3: 'Warning'
            }

            return oStatus[iValue]
        },

        formatDate: function(sDate) {
            const oDate = new Date(sDate)
            return oDate.toLocaleDateString()
        }
    })
})