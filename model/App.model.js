sap.ui.define([
    "sap/ui/model/json/JSONModel"
], (JSONModel) => {

    return {
        createModel: () => {
            const oData = {
                users: [
                    {
                        name: "Diana",
                        adress: {
                            street: 'Müllerstrase 12',
                            postal: '12345'
                        },
                        age: 22
                    },
                    {
                        name: "Brahim",
                        adress: {
                            street: 'Hans-Baumannstraße 65',
                            postal: '12345'
                        },
                        age: 30
                    },
                    {
                        name: "Manuel",
                        adress: {
                            street: 'Gustav-Gans Weg 13',
                            postal: '12345'
                        },
                        age: 35
                    },
                    {
                        name: 'Stephan',
                        adress: {
                            street: 'Herman-Müller Weg 76',
                            postal: '12345'
                        },
                        age: 55
                    },
                    {
                        name: "Lukas",
                        adress: {
                            street: 'Müllerstrase 12',
                            postal: '12345'
                        },
                        age: 22
                    },
                    {
                        name: "Winfred",
                        adress: {
                            street: 'Hans-Baumannstraße 65',
                            postal: '12345'
                        },
                        age: 30
                    },
                    {
                        name: "Helmut",
                        adress: {
                            street: 'Gustav-Gans Weg 13',
                            postal: '12345'
                        },
                        age: 35
                    },
                    {
                        name: 'Anton',
                        adress: {
                            street: 'Herman-Müller Weg 76',
                            postal: '12345'
                        },
                        age: 55
                    }
                ]
            }

            const oModel = new JSONModel(oData)
            return oModel
        }
    }

})
