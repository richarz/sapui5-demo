sap.ui.define([
    "sap/ui/core/UIComponent"
], (UIComponent) => {
    return UIComponent.extend("demo.Component", {
        metadata: {
            // get metadata of our App from "manifest.json"
            manifest: 'json'
        },

        // Initialize AppComponent
        init: function() {
            // Class constructor of super-class
            UIComponent.prototype.init.apply(this, arguments)

            // init URL-Routing: url/hash
            this.getRouter().initialize()
        }
    })
})